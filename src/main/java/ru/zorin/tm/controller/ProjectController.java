package ru.zorin.tm.controller;

import ru.zorin.tm.api.controller.IProjectController;
import ru.zorin.tm.api.service.IAuthService;
import ru.zorin.tm.api.service.IProjectService;
import ru.zorin.tm.error.invalid.InvalidIndexException;
import ru.zorin.tm.error.project.ProjectEmptyException;
import ru.zorin.tm.error.project.ProjectUpdateException;
import ru.zorin.tm.entity.Project;
import ru.zorin.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    private final IAuthService authService;

    public ProjectController(IProjectService projectService, IAuthService authService) {
        this.projectService = projectService;
        this.authService = authService;

    }

    private void showProject(Project project) {
        if (project == null) throw new ProjectEmptyException();
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
    }

    @Override
    public void showProjects() {
        final String userId = authService.getUserId();
        System.out.println("[LIST PROJECT]");
        final List<Project> projects = projectService.findAll(userId);
        for (Project project : projects) System.out.println(project);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void clearProjects() {
        final String userId = authService.getUserId();
        System.out.println("[CLEAR PROJECT]");
        projectService.clear(userId);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void createProject() {
        final String userId = authService.getUserId();
        System.out.println("[CREATE PROJECT]");
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        projectService.create(userId, name, description);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void showProjectById() {
        final String userId = authService.getUserId();
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findProjectById(userId, id);
        if (project == null) throw new ProjectEmptyException();
        showProject(project);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void showProjectByIndex() {
        final String userId = authService.getUserId();
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX:");
        try {
            final Integer index = TerminalUtil.nextNumber() - 1;
            final Project project = projectService.findProjectByIndex(userId, index);
            if (project == null) throw new ProjectEmptyException();
            showProject(project);
            System.out.println("[COMPLETE]");
        } catch (IndexOutOfBoundsException e) {
            throw new InvalidIndexException();
        }
    }

    @Override
    public void showProjectByName() {
        final String userId = authService.getUserId();
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findProjectByName(userId, name);
        if (project == null) throw new ProjectEmptyException();
        showProject(project);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void updateProjectById() {
        final String userId = authService.getUserId();
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findProjectById(userId, id);
        if (project == null) throw new ProjectEmptyException();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateProjectById(userId, id, name, description);
        if (projectUpdated == null) throw new ProjectUpdateException();
        System.out.println("[COMPLETE]");
    }

    @Override
    public void updateProjectByIndex() {
        final String userId = authService.getUserId();
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX:");
        try {
            final Integer index = TerminalUtil.nextNumber() - 1;
            final Project project = projectService.findProjectByIndex(userId, index);
            if (project == null) throw new ProjectEmptyException();
            System.out.println("ENTER NAME:");
            final String name = TerminalUtil.nextLine();
            System.out.println("[ENTER DESCRIPTION]");
            final String description = TerminalUtil.nextLine();
            final Project projectUpdated = projectService.updateProjectByIndex(userId, index, name, description);
            if (projectUpdated == null) throw new ProjectUpdateException();
        } catch (IndexOutOfBoundsException e) {
            throw new InvalidIndexException();
        }
    }

    @Override
    public void removeProjectById() {
        final String userId = authService.getUserId();
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.removeProjectById(userId, id);
        if (project == null) throw new ProjectEmptyException();
        else System.out.println("[COMPLETE]");
    }

    @Override
    public void removeProjectByName() {
        final String userId = authService.getUserId();
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.removeProjectByName(userId, name);
        if (project == null) throw new ProjectEmptyException();
        else System.out.println("[COMPLETE]");
    }

    @Override
    public void removeProjectByIndex() {
        final String userId = authService.getUserId();
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER INDEX:");
        try {
            final Integer index = TerminalUtil.nextNumber() - 1;
            final Project project = projectService.removeProjectByIndex(userId, index);
            if (project == null) throw new ProjectEmptyException();
            else System.out.println("[COMPLETE]");
        } catch (IndexOutOfBoundsException e) {
            throw new InvalidIndexException();
        }
    }
}