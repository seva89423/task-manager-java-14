package ru.zorin.tm.controller;

import ru.zorin.tm.api.controller.IUserController;
import ru.zorin.tm.api.service.IAuthService;
import ru.zorin.tm.api.service.IUserService;
import ru.zorin.tm.entity.User;
import ru.zorin.tm.error.invalid.InvalidLoginException;
import ru.zorin.tm.error.invalid.InvalidUserIdException;
import ru.zorin.tm.util.HashUtil;
import ru.zorin.tm.util.TerminalUtil;

public class UserController implements IUserController {

    private final IAuthService authService;
    private IUserService userService;

    public UserController(IUserService userService, IAuthService authService) {
        this.userService = userService;
        this.authService = authService;
    }

    @Override
    public void updateUserById() {
        final String userId = authService.getUserId();
        System.out.println("[UPDATE USER]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final User user = userService.findById(id);
        if (user == null) throw new InvalidUserIdException();
        System.out.println("ENTER NEW LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        final User userUpdated = userService.updateById(userId, id, login, HashUtil.salt(password));
        if (userUpdated == null) throw new InvalidUserIdException();
        System.out.println("[COMPLETE]");
    }

    @Override
    public void showUserById() {
        final String userId = authService.getUserId();
        System.out.println("[SHOW USER]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final User user = userService.findById(id);
        if (user == null) {
            System.out.println("[ERROR]");
            return;
        }
        showUser(user);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void showUserByLogin() {
        final String userId = authService.getUserId();
        System.out.println("[SHOW USER]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        final User user = userService.findByLogin(login);
        if (user == null) {
            System.out.println("[ERROR]");
            return;
        }
        showUser(user);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void showUser(User user) {
        if (user == null) return;
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
    }

    @Override
    public void updateUserByLogin() {
        System.out.println("[UPDATE USER]");
        System.out.println("ENTER OLD LOGIN:");
        final String login = TerminalUtil.nextLine();
        final String userId = authService.getUserId();
        final User user = userService.findByLogin(login);
        if (user == null) throw new InvalidLoginException();
        System.out.println("ENTER NEW LOGIN:");
        final String newLogin = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        final User userUpdated = userService.updateByLogin(userId, newLogin, HashUtil.salt(password));
        if (userUpdated == null) throw new InvalidUserIdException();
        System.out.println("[COMPLETE]");
    }

    @Override
    public void removeUserById() {
        final String userId = authService.getUserId();
        System.out.println("[REMOVE USER]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final User user = userService.removeById(userId, id);
        if (user == null) throw new InvalidLoginException();
        System.out.println("[REMOVED]");
        authService.logout();
    }

    @Override
    public void removeUserByLogin() {
        final String userId = authService.getUserId();
        System.out.println("[REMOVE USER]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        final User user = userService.removeByLogin(userId, login);
        if (user == null) throw new InvalidLoginException();
        System.out.println("[REMOVED]");
        authService.logout();
    }
}