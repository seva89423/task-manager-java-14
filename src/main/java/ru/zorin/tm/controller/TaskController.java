package ru.zorin.tm.controller;

import ru.zorin.tm.api.controller.ITaskController;
import ru.zorin.tm.api.service.IAuthService;
import ru.zorin.tm.api.service.ITaskService;
import ru.zorin.tm.error.invalid.InvalidIndexException;
import ru.zorin.tm.error.task.TaskEmptyException;
import ru.zorin.tm.error.task.TaskUpdateException;
import ru.zorin.tm.entity.Task;
import ru.zorin.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    public ITaskService taskService;

    private final IAuthService authService;

    public TaskController(final ITaskService taskService, IAuthService authService) {
        this.taskService = taskService;
        this.authService = authService;
    }

    @Override
    public void showTasks() {
        final String userId = authService.getUserId();
        System.out.println("[LIST TASKS]");
        final List<Task> tasks = taskService.findAll(userId);
        for (Task task : tasks) System.out.println(task);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void clearTasks() {
        final String userId = authService.getUserId();
        System.out.println("[CLEAR TASKS]");
        taskService.clear(userId);
        System.out.println("[COMPLETE]");
    }

    private void showTask(Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
    }

    @Override
    public void createTask() {
        final String userId = authService.getUserId();
        System.out.println("[CREATE TASK]");
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        taskService.create(userId, name, description);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void showTaskById() {
        final String userId = authService.getUserId();
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(userId, id);
        if (task == null) {
            System.out.println("[ERROR]");
            return;
        }
        showTask(task);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void showTaskByIndex() {
        final String userId = authService.getUserId();
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER INDEX:");
        try {
            final Integer index = TerminalUtil.nextNumber() - 1;
            final Task task = taskService.findOneByIndex(userId, index);
            if (task == null) throw new TaskEmptyException();
            showTask(task);
            System.out.println("[COMPLETE]");
        } catch (IndexOutOfBoundsException e) {
            throw new InvalidIndexException();
        }
    }

    @Override
    public void showTaskByName() {
        final String userId = authService.getUserId();
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.findOneByName(userId, name);
        if (task == null) throw new TaskEmptyException();
        showTask(task);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void updateTaskById() {
        final String userId = authService.getUserId();
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(userId, id);
        if (task == null) throw new TaskEmptyException();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = taskService.updateTaskById(userId, id, name, description);
        if (taskUpdated == null) throw new TaskUpdateException();
        System.out.println("[COMPLETE]");
    }

    @Override
    public void updateTaskByIndex() {
        final String userId = authService.getUserId();
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER INDEX:");
        try {
            final Integer index = TerminalUtil.nextNumber() - 1;
            final Task task = taskService.findOneByIndex(userId, index);
            if (task == null) throw new TaskEmptyException();
            System.out.println("ENTER NAME:");
            final String name = TerminalUtil.nextLine();
            System.out.println("[ENTER DESCRIPTION]");
            final String description = TerminalUtil.nextLine();
            final Task taskUpdated = taskService.updateTaskByIndex(userId, index, name, description);
            if (taskUpdated == null) throw new TaskUpdateException();
        } catch (IndexOutOfBoundsException e) {
            throw new InvalidIndexException();
        }
    }

    @Override
    public void removeTaskById() {
        final String userId = authService.getUserId();
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.removeOneById(userId, id);
        if (task == null) throw new TaskEmptyException();
        else System.out.println("[COMPLETE]");
    }


    @Override
    public void removeTaskByName() {
        final String userId = authService.getUserId();
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.removeOneByName(userId, name);
        if (task == null) throw new TaskEmptyException();
        System.out.println("[COMPLETE]");
    }

    @Override
    public void removeTaskByIndex() {
        final String userId = authService.getUserId();
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER INDEX:");
        try {
            final Integer index = TerminalUtil.nextNumber() - 1;
            final Task task = taskService.removeOneByIndex(userId, index);
            if (task == null) throw new TaskEmptyException();
            if (index == null || index < 0) throw new InvalidIndexException();
            System.out.println("[COMPLETE]");
        } catch (IndexOutOfBoundsException e) {
            throw new InvalidIndexException();
        }
    }
}