package ru.zorin.tm.api.repository;

import ru.zorin.tm.dto.Command;

public interface ICommandRepository {

    String[] getCommands(Command... values);

    String[] getArgs(Command... values);

    Command[] getTerminalCommands();

    String[] getCommands();

    String[] getArgs();

}
