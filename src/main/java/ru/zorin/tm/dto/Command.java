package ru.zorin.tm.dto;

import ru.zorin.tm.constant.ArgumentConst;
import ru.zorin.tm.constant.TerminalConst;

public class Command {

    public static final Command HELP = new Command(
            TerminalConst.CMD_HELP, ArgumentConst.ARG_HELP, "Show developer info"
    );

    public static final Command ABOUT = new Command(
            TerminalConst.CMD_ABOUT, ArgumentConst.ARG_ABOUT, "Show version info"
    );

    public static final Command VERSION = new Command(
            TerminalConst.CMD_VERSION, ArgumentConst.ARG_VERSION, "Show display message"
    );

    public static final Command INFO = new Command(
            TerminalConst.CMD_INFO, ArgumentConst.ARG_INFO, "Show display message"
    );

    public static final Command EXIT = new Command(
            TerminalConst.CMD_EXIT, null, "Close application"
    );

    public static final Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE, null, "Create new task"
    );

    public static final Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR, null, "Remove all tasks"
    );

    public static final Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST, null, "Show task list"
    );

    public static final Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE, null, "Create new project"
    );

    public static final Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null, "Remove all projects"
    );

    public static final Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST, null, "Show task project"
    );

    public static final Command TASK_UPDATE_BY_INDEX = new Command(
            TerminalConst.TASK_UPDATE_BY_INDEX, null, "Update task by index"
    );

    public static final Command TASK_UPDATE_BY_ID = new Command(
            TerminalConst.TASK_UPDATE_BY_ID, null, "Update task by id"
    );

    public static final Command TASK_VIEW_BY_ID = new Command(
            TerminalConst.TASK_VIEW_BY_ID, null, "Show task using id"
    );
    public static final Command TASK_VIEW_BY_NAME = new Command(
            TerminalConst.TASK_VIEW_BY_NAME, null, "Show task using name"
    );

    public static final Command TASK_VIEW_BY_INDEX = new Command(
            TerminalConst.TASK_VIEW_BY_INDEX, null, "Show task using index"
    );

    public static final Command TASK_REMOVE_BY_ID = new Command(
            TerminalConst.TASK_REMOVE_BY_ID, null, "Remove task using id"
    );
    public static final Command TASK_REMOVE_BY_NAME = new Command(
            TerminalConst.TASK_REMOVE_BY_NAME, null, "Remove task using name"
    );

    public static final Command TASK_REMOVE_BY_INDEX = new Command(
            TerminalConst.TASK_REMOVE_BY_INDEX, null, "Remove task using index"
    );

    public static final Command PROJECT_UPDATE_BY_INDEX = new Command(
            TerminalConst.PROJECT_UPDATE_BY_INDEX, null, "Update project by index"
    );

    public static final Command PROJECT_UPDATE_BY_ID = new Command(
            TerminalConst.PROJECT_UPDATE_BY_ID, null, "Update project by id"
    );

    public static final Command PROJECT_VIEW_BY_ID = new Command(
            TerminalConst.PROJECT_VIEW_BY_ID, null, "Show project using id"
    );
    public static final Command PROJECT_VIEW_BY_NAME = new Command(
            TerminalConst.PROJECT_VIEW_BY_NAME, null, "Show project using name"
    );

    public static final Command PROJECT_VIEW_BY_INDEX = new Command(
            TerminalConst.PROJECT_VIEW_BY_INDEX, null, "Show project using index"
    );

    public static final Command PROJECT_REMOVE_BY_ID = new Command(
            TerminalConst.PROJECT_REMOVE_BY_ID, null, "Remove project using id"
    );
    public static final Command PROJECT_REMOVE_BY_NAME = new Command(
            TerminalConst.PROJECT_REMOVE_BY_NAME, null, "Remove project using name"
    );

    public static final Command PROJECT_REMOVE_BY_INDEX = new Command(
            TerminalConst.PROJECT_REMOVE_BY_INDEX, null, "Remove project using index"
    );

    public static final Command LOGIN = new Command(
            TerminalConst.LOGIN, null, "Log in the system"
    );

    public static final Command LOGOUT = new Command(
            TerminalConst.LOGOUT, null, "Log out the system"
    );

    public static final Command REGISTRY = new Command(
            TerminalConst.REGISTRY, null, "Create new profile in the system"
    );

    public static final Command USER_UPDATE_BY_ID = new Command(
            TerminalConst.USER_UPDATE_BY_ID, null, "Update user using id"
    );

    public static final Command USER_UPDATE_BY_LOGIN = new Command(
            TerminalConst.USER_UPDATE_BY_LOGIN, null, "Update user using login"
    );

    public static final Command USER_SHOW_BY_ID = new Command(
            TerminalConst.USER_SHOW_BY_ID, null, "Show user using id"
    );

    public static final Command USER_SHOW_BY_LOGIN = new Command(
            TerminalConst.USER_SHOW_BY_LOGIN, null, "Show user using login"
    );

    public static final Command USER_REMOVE_BY_ID = new Command(
            TerminalConst.USER_REMOVE_BY_ID, null, "Show user using id"
    );

    public static final Command USER_REMOVE_BY_LOGIN = new Command(
            TerminalConst.USER_REMOVE_BY_LOGIN, null, "Show user using login"
    );
    private String name = "";

    private String arg = "";

    private String description = "";

    public Command() {

    }

    public Command(String name) {
        this.name = name;
    }

    public Command(String name, String arg) {
        this.name = name;
        this.arg = arg;
    }

    public Command(String name, String arg, String description) {
        this.name = name;
        this.arg = arg;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArg() {
        return arg;
    }

    public void setArg(String arg) {
        this.arg = arg;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        final StringBuilder result = new StringBuilder();
        if (name != null && !name.isEmpty()) result.append(name);
        if (arg != null && !arg.isEmpty()) result.append(", ").append(arg);
        if (description != null && !description.isEmpty()) result.append(": ").append(description);
        return result.toString();
    }
}